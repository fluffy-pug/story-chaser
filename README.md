# Story chaser

> Create a story in real-time with your friends by referring to shown images.

<dl>
  <a href='https://i.imgur.com/bivQOMK.png'>
    <img src='https://i.imgur.com/bivQOMK.png' width='250px'></img>
  </a>
</dl>

**Live demo available [HERE](https://swroblewski.pl/story-chaser)**  
To truly play this game, feed `sources` array located in `./src/data/Storage.js` with links to images you want to use and... that's it!

## Build from source

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

Project generated in vue-cli.
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
