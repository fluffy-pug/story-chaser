export default {
    players: [],

    settings: {
        // These values are default
        timeToAnswer: 5000,
        winScore: 10,
        endless: false,
        progressTick: 10
    },

    sources: [
        "http://via.placeholder.com/400x250"
    ],

    questions: [
        'a',
        'b',
        'c',
        'd',
    ]
}