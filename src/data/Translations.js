export default {
    'pl': {
        lobby: {
            title: 'Lobby',
            players: {
                title: 'Gracze',
                none: 'Brak graczy',
                add: 'Dodaj gracza',
                table: {
                    name: 'Imię',
                    actions: 'Akcje'
                },
                popup: {
                    addPlayer: {
                        message: 'Wprowadź imię gracza',
                        placeholder: 'np. PJOTER',
                        cancel: 'Anuluj',
                        add: 'Dodaj'
                    },
                    editPlayer: {
                        message: 'Wprowadź nowe imię',
                        placeholder: 'np. Kacperek',
                        cancel: 'Anuluj',
                        save: 'Zapisz'
                    },
                    removePlayer: {
                        message: 'Czy na pewno chcesz usunąć gracza',
                        cancel: 'Anuluj',
                        remove: 'Tak, usuń'
                    },
                    notEnough: {
                        title: 'Dziki error',
                        message: 'Aby rozpocząć grę, dodaj co najmniej dwóch graczy.'
                    },
                    wrongTime: {
                        title: 'Dziki error',
                        message: 'Ustaw co najmniej sekundę na odpowiedź!'
                    }
                },
                info: {
                    newPlayer: 'dodany/a do gry!',
                    editPlayer: 'to teraz',
                    removePlayer: 'usunięty/a z gry'
                }
            },
            settings: {
                title: 'Ustawienia gry',
                time: 'Czas na odpowiedź (s)',
                score: 'Punkty do wygranej',
                endless: 'Bez końca'
            }            
        },
        game: {
            skip: 'Pomiń',
            assign: 'Dodaj punkt',
            tooltip: 'Możesz również przydzielać punkty wciskając spację i pomijać pytania strzałką w prawo'
        },
        result: {
            title: 'Howdy!',
            question: 'Co teraz?',
            return: 'Powróć do lobby',
            continue: 'Kontynuuj',
            tooltip: 'Kontynuacja gry uruchamia tryb endless, dzięki czemu gra nigdy się nie zakończy.',

            noQuestionsLeft: {
                title: 'Howdy!',
                message: 'Wszystkie pytania zostały wykorzystane!',
            }
        },
    },

    'en': {
        // TODO
    }
}